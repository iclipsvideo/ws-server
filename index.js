var port = process.env.PORT || 8080;
//var ip = process.env.IP || "127.0.0.1";

var sql = require("sql.js");
var util = require("util");
var express = require("express");
var http = require("http");
var url = require("url");
var WebSocket = require("ws");
var cors = require("cors");
var app = express();

// db setup
// ********
var db = new sql.Database();
var sqlstr =
  "CREATE TABLE log (id INTEGER PRIMARY KEY, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, message text);";
db.run(sqlstr);

app.get("/log", function(req, res) {
  var html = "";
  html +=
    '<style> #t { font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; } #t td, #t th { border: 1px solid #ddd; padding: 8px; } #t tr:nth-child(even){background-color: #f2f2f2;} #t tr:hover {background-color: #ddd;} #t th { padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; color: white; } </style>';
  html += '<table id="t">';
  html += "  <tr>";
  html += "    <th>Log Id</th>";
  html += "    <th>Time</th>";
  html += "    <th>Message</th>";
  html += "  </tr>";

  db.each(
    "SELECT id, time, message FROM (SELECT id, time, message FROM log ORDER BY id DESC LIMIT 1000) ORDER BY id ASC;",
    {},
    function(row) {
      html += util.format(
        "<tr><td>%s</td><td>%s</td><td>%s</td></tr>",
        row.id,
        row.time,
        row.message
      );
    }
  );

  html += "</table>";
  res.send(html);
});

var httpServer = http.createServer(app);
var wss = new WebSocket.Server({
  server: httpServer,
  clientTracking: true
});

httpServer.listen(port, function listening() {
  log("Listening on " + httpServer.address().port);
});

var ListOfClients = [];

wss.on("connection", function connection(ws, req) {
  var location = url.parse(req.url, true);
  var ip = req.connection.remoteAddress;

  log("Someone has connected! " + ip);

  ws.on("message", function incoming(jsonMessage) {
    log("received: " + jsonMessage);

    var json = JSON.parse(jsonMessage);

    // {
    //  action:   'add user',
    //  username: '',
    //  message: ''
    // }

    switch (json.action) {
      case "add user":
        var client = {
          username: json.username,
          socket: ws
        };
        ListOfClients.push(client);
        var m = "user has join: " + json.username;

        // Broadcast to all (except current client)
        wss.clients.forEach(function each(client) {
          if (client !== ws && client.readyState === WebSocket.OPEN) {
            client.send(m);
          }
        });

        // Send to current client
        ws.send(m);

        break;
      case "send message":
        // Broadcast to all (except current client)
        wss.clients.forEach(function each(client) {
          if (client !== ws && client.readyState === WebSocket.OPEN) {
            client.send(json.message);
          }
        });
        break;
    }
  });

  // 1. doc says theres no parms. is it?
  // 2. can i use anonymous function?
  ws.on("close", function(code, number) {
    for (i = 0; i < ListOfClients.length; i++) {
      if (ListOfClients[i].socket === ws) {
        var u = ListOfClients[i].username;
        ListOfClients.splice(i, 1);

        var m = "client has disconnected: " + u;

        // Broadcast to all (except current client)
        wss.clients.forEach(function each(client) {
          if (client !== ws && client.readyState === WebSocket.OPEN) {
            client.send(m);
          }
        });

        log(m);
      }
    }
  });

  // - can i use anonymous function?
  ws.on("error", function(err) {
    log("errored: " + err);
  });
});

////////////////////////////////////////
// Functions

function getClient(websocket) {
  for (i = 0; i < ListOfClients.length; i++) {
    if (ListOfClients[i].socket === ws) {
      clients.splice(i, 1);
      console.log("remove client");
    }
  }
}

function broadcastMessage(sock, msg) {
  wss.clients.forEach(function each(client) {
    if (client !== sock && client.readyState === WebSocket.OPEN) {
      client.send(msg);
    }
  });
}

function log(message) {
  console.log(message);
  var sqlstr =
    "INSERT INTO log (id, message) VALUES (NULL, '" + message + "');";
  db.run(sqlstr);
}
